package com.datatheorem.xposedtest

import android.content.Context
import androidx.compose.ui.text.input.TextFieldValue
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.*
import okhttp3.*
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

data class SimpleHttpResponse(val responseString: String)
object HttpHelpers {

    private val okHttpClient = OkHttpClient.Builder().build()
    private fun getRequest(httpUrlToTest: String): Request = Request.Builder()
        .url(httpUrlToTest)
        .build()

    suspend fun okHttpSync(httpUrlToTest: String): SimpleHttpResponse {
        return withContext(Dispatchers.IO) {
            okHttpClient.newCall(getRequest(httpUrlToTest)).execute().use {
                it.toSimpleResponse()
            }
        }
    }

    suspend fun okHttpASync(httpUrlToTest: String): SimpleHttpResponse {
        return withContext(Dispatchers.IO) {
            suspendCoroutineWithTimeout { continuation ->
               okHttpClient.newCall(getRequest(httpUrlToTest)).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                        continuation.resumeWithException(e)
                    }

                    override fun onResponse(call: Call, response: Response) {
                        response.use {
                            continuation.resume(response.toSimpleResponse())
                        }
                    }
                })
            }
        }
    }

    private fun Response.toSimpleResponse() = SimpleHttpResponse(this.body?.string() ?: "")

    suspend fun testVolley(context: Context, httpUrlToTest: String): SimpleHttpResponse {
        return withContext(Dispatchers.IO) {
            suspendCoroutineWithTimeout { continuation ->
                val queue = Volley.newRequestQueue(context)

                val stringRequest = StringRequest(com.android.volley.Request.Method.GET, httpUrlToTest,
                    { response ->
                        continuation.resume(SimpleHttpResponse(response))
                    },
                    { continuation.resumeWithException(Throwable(it)) })

                queue.add(stringRequest)
            }
        }
    }


    suspend fun testHTTPUrlConnection(httpUrlToTest: String): SimpleHttpResponse {
        return withContext(Dispatchers.IO) {
            val urlConnection = URL(httpUrlToTest).openConnection() as HttpURLConnection
            urlConnection.apply {
                requestMethod = "GET"
                setRequestProperty("Content-Type", "application/json")
            }
            urlConnection.disconnect()

            urlConnection.inputStream.bufferedReader().use {
                SimpleHttpResponse(it.readText())
            }
        }
    }
}