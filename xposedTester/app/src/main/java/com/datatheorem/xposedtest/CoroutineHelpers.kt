package com.datatheorem.xposedtest

import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.withTimeout

suspend inline fun <T> suspendCoroutineWithTimeout(
    timeoutMillis: Long = 30_000,
    crossinline block: (CancellableContinuation<T>) -> Unit
) = withTimeout(timeoutMillis) {
    suspendCancellableCoroutine(block = block)
}