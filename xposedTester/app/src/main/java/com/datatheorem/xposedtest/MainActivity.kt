package com.datatheorem.xposedtest

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CutCornerShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.datatheorem.xposedtest.ui.theme.XposedTesterTheme
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MainActivityComposable()
        }
    }
}

@Composable
fun MainActivityComposable() {
    val context = LocalContext.current
    var httpUrl by remember { mutableStateOf("https://629c9f513798759975d89f93.mockapi.io/api/testData") }


    XposedTesterTheme {
        // A surface container using the 'background' color from the theme
        Surface(
            modifier = Modifier
                .fillMaxSize()
                .padding(8.dp), color = MaterialTheme.colors.background
        ) {
            Column(verticalArrangement = Arrangement.spacedBy(10.dp)) {
                TextField(value = httpUrl, onValueChange = { newText -> httpUrl = newText })
                Spacer(modifier = Modifier.size(24.dp))
                HttpCheckerComposable("OkHttp test sync") { HttpHelpers.okHttpSync(httpUrl) }
                HttpCheckerComposable("OkHttp test async") { HttpHelpers.okHttpASync(httpUrl) }
                HttpCheckerComposable("Volley test") { HttpHelpers.testVolley(context, httpUrl) }
                HttpCheckerComposable("HttpUrlConnection test") { HttpHelpers.testHTTPUrlConnection(httpUrl) }
            }
        }
    }
}


@Composable
fun HttpCheckerComposable(buttonText: String, httpCall: suspend () -> SimpleHttpResponse) {
    val coroutineScope = rememberCoroutineScope()
    var httpResponse by remember { mutableStateOf(TextFieldValue("Http response will go here")) }

    val doHttp: () -> Unit = {
        coroutineScope.launch {
            httpResponse = TextFieldValue(httpCall().toString())
        }
    }

    Column() {
        Button(onClick = doHttp, shape = CutCornerShape(10)) {
            Text(text = buttonText)
        }
        Spacer(modifier = Modifier.size(4.dp))

        BasicTextField(
            httpResponse,
            { httpResponse = it },
            modifier = Modifier
                .height(50.dp)
                .scrollable(rememberScrollableState { 0f }, Orientation.Vertical),
            decorationBox = {
                Box(
                    Modifier
                        .fillMaxWidth()
                        .border(2.dp, Color(0xF0101400), RoundedCornerShape(20.dp))
                        .background(Color(0x2596BEAF))
                        .padding(16.dp)
                ) { it() }
            },
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    MainActivityComposable()
}