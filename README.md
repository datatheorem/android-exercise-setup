# Creating an android emulator that supports Xposed hooks

We recommend using the the [rootAVD script](https://github.com/newbit1/rootAVD) to create a rooted emulator

- First create an emulator running Android 12 (you may chose another OS version, but compatibility may vary) and launch it, make sure this is the only device connected to adbd.
- Then using rootAVD run `./rootAVD.sh`
This will give you a list of command and you should run the first one that will look like `./rootAVD.sh PATH_TO_SYSTEM_IMAGES/google_apis/arm64-v8a/ramdisk.img`
The Emulator should shut down automatically after this step, if it doesn't, manually do it.
- Start the emulator and open the magisk app, the app should be in the list of all apps, this will prompt you to finish its installation and will reboot
- After rebooting, go to the magisk app and its settings and check the Zygisk support switch
- Reboot one last time 
  
Now we have a rooted emulator, we will need to install xposed. 

- LSPosed can be found in this repository [here](./LSPosed1.8.6.zip)
- Add the zip to your emulator
- Install LSPosed from magisk by going to Modules > Install from storage and selecting the LSPosed zip.




# Xposed 
As Xposed framework is hosted on a deprecated repository, its availability can be flaky so you can find the [xposed framework jar in this repository](./xposed-api-82.jar).